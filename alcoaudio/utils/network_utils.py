import os

import numpy as np
import torch
from sklearn.metrics import recall_score, precision_recall_fscore_support
from torch import tensor


def to_tensor(x, device=None):
    if device is None:
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    return tensor(x).to(device=device).float()


def to_numpy(x):
    if isinstance(x, torch.Tensor):
        if x.requires_grad:
            return x.detach().cpu().numpy()
        else:
            return x.cpu().numpy()
    else:
        return x
        