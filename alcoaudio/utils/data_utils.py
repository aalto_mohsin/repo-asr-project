import pandas as pd
import random
import h5py
import numpy as np

def save_h5py(data, filename, dataset_name='data'):
    print('Saving data in ', filename)
    h5f = h5py.File(filename, 'w')
    h5f.create_dataset(dataset_name, data=data)
    h5f.close()


def save_npy(data, filename):
    print('Saving data in ', filename)
    np.save(filename, data)


def read_npy(filename):
    print("Reading data from file ", filename)
    return np.load(filename, allow_pickle=True)


def save_csv(data, columns, filename):
    print('Saving CSV file in ', filename)
    df = pd.DataFrame(data, columns=columns)
    df.to_csv(filename, index=False)
